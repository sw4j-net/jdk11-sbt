# Introduction

This project creates a docker image with (open)jdk 11 and sbt installed.

The image can be used to run sbt builds.

The original repository is at https://git.sw4j.net/sw4j-net/jdk11-sbt

This repository is mirrored to https://gitlab.com/sw4j-net/jdk11-sbt
